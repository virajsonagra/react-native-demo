import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Image,
  TouchableOpacity,
  Text,
  BackHandler,
  ToastAndroid
} from "react-native";
// import { Button, Icon, Input } from "react-native-elements";
import GeneralStatusBarColor from "../../components/GeneralStatusBarColor";

class HomeScreen extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <GeneralStatusBarColor
          backgroundColor="#ffffff"
          barStyle="dark-content"
        />
        <Image
          source={require("../../../assets/logo.png")}
          style={styles.logo}
        />
        <View style={{ height: 50 }} />
        <View style={styles.btn}>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate("Login")}
            style={styles.button1}
            underlayColor="#fff"
            backgroundColor="#fff"
          >
            <Text style={styles.button1Text}>Sign In</Text>
          </TouchableOpacity>
        </View>
        <View style={{ height: 10 }} />
        <View style={styles.btn}>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate("Signup")}
            style={styles.button2}
            underlayColor="#fff"
            backgroundColor="#48B0FE"
          >
            <Text style={styles.button2Text}>Sign Up</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  button1Text: {
    color: "#ffffff",
    padding: 10,
    textAlign: "center",
    fontSize: 18
  },
  button2Text: {
    color: "#48B0FE",
    padding: 10,
    textAlign: "center",
    fontSize: 18
  },
  button1: {
    flex: 1,
    position: "absolute",
    width: "100%",
    borderWidth: 1,
    borderColor: "#48B0FE",
    borderRadius: 50,
    shadowColor: "grey",
    backgroundColor: "#48B0FE"
  },
  button2: {
    flex: 1,
    position: "absolute",
    width: "100%",
    borderWidth: 1,
    borderRadius: 50,
    shadowColor: "grey",
    backgroundColor: "#ffff",
    borderColor: "#48B0FE"
  },
  logo: {
    width: 150,
    height: 150
  },
  container: {
    flex: 1,
    flexDirection: "column",
    backgroundColor: "#ffffff",
    padding: 20,
    alignItems: "center",
    justifyContent: "center"
  },

  title: {
    color: "#ffffff",
    textAlign: "center",
    fontSize: 18,
    width: "100%",
    marginBottom: 10
  },
  btn: {
    // flex: 1
    width: "80%",
    height: 40,
    marginBottom: 10
  }
});
export default HomeScreen;
