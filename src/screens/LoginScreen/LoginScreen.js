import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Alert,
  TouchableOpacity,
  TouchableHighlight,
  TouchableWithoutFeedback,
  Keyboard,
  Image,
  Linking
} from "react-native";
import { Input } from "react-native-elements";
import GeneralStatusBarColor from "../../components/GeneralStatusBarColor";
// import SplashScreen from "react-native-splash-screen";
class LoginScreen extends React.Component {
  render() {
    return (
      <View style={styles.loginContainerWrapper}>
        <GeneralStatusBarColor
          backgroundColor="#48B0FE"
          barStyle="light-content"
        />
        <View style={styles.loginContainerTopHalf}>
          <Image
            source={require("../../../assets/logo.png")}
            style={styles.logo}
          />
        </View>
        <View style={styles.loginContainerBottomHalf}>
          <Text style={{ marginEnd: 15 }}>
            Don't have account?
            <Text> </Text>
            <Text
              style={{ color: "#48B0FE", paddingHorizontal: 15 }}
              onPress={() => this._onPressSignup()}
            >
              Sign-Up
            </Text>
          </Text>
        </View>
        <View style={styles.loginBoxContainer}>
          <View style={styles.loginBoxContainerInner}>
            <View style={styles.loginInputSection}>
              <Input
                style={styles.input}
                keyboardType="email-address"
                placeholder="Email"
                placeholderTextColor="grey"
                rightIcon={{
                  type: "font-awesome",
                  name: "envelope-o",
                  size: 15,
                  color: "gray"
                }}
              />
            </View>
            <View style={styles.loginInputSection}>
              <Input
                secureTextEntry={true}
                style={styles.input}
                width={100}
                placeholder="Password"
                rightIcon={{
                  type: "font-awesome",
                  name: "eye",
                  size: 15,
                  color: "gray"
                }}
              />
            </View>
            <TouchableOpacity
              style={styles.loginInputSectionButton}
              underlayColor="#fff"
              backgroundColor="#48B0FE"
              onPress={() => this._onPressSignin()}
            >
              <Text style={styles.loginButtonText}>Sign In</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  signupLink: {
    flex: 1,
    position: "absolute",
    width: "80%"
    // top: 220
  },
  logo: {
    width: 150,
    height: 150
  },
  input: {
    flex: 1,
    padding: 0
  },
  loginInputSection: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "center",
    paddingHorizontal: 15,
    padding: 5,
    marginBottom: 0
  },
  loginInputSectionPassword: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    top: 0,
    width: "100%",
    paddingHorizontal: 15
  },
  loginButtonText: {
    color: "#fff",
    padding: 10,
    textAlign: "center",
    fontSize: 18
  },
  loginInputSectionButton: {
    flex: 1,
    // width: "100%",
    borderRadius: 50,
    shadowColor: "grey",
    // justifyContent: "center",
    // alignItems: "center",
    // textAlign: "center",
    paddingHorizontal: 70,
    backgroundColor: "#48B0FE",
    marginTop: 20,
    marginBottom: 20
  },
  loginBoxContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    width: "75%",
    position: "absolute",
    zIndex: 9999,
    top: "42%",
    height: "auto",
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
    backgroundColor: "#ffffff",
    borderRadius: 5,
    borderWidth: 1,
    borderColor: "rgba(128,128,128,0.1)",
    paddingHorizontal: 10
  },
  loginBoxContainerInner: {
    height: "auto",
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
    marginBottom: 5,
    marginTop: 10
  },
  loginContainerWrapper: {
    flex: 1
  },
  loginContainerTopHalf: {
    flex: 1,
    backgroundColor: "#48B0FE",
    justifyContent: "center",
    alignItems: "center"
  },
  loginContainerBottomHalf: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  }
});
export default LoginScreen;
