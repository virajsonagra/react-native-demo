import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Alert,
  TouchableOpacity,
  TouchableHighlight,
  TouchableWithoutFeedback,
  Keyboard,
  Image,
  Linking
} from "react-native";
import { Input } from "react-native-elements";
import GeneralStatusBarColor from "../../components/GeneralStatusBarColor";
class SignupScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      fname: "",
      lname: "",
      email: "",
      password: ""
    };
  }
  validateEmail = text => {
    console.log(text);
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (reg.test(text) === false) {
      return false;
    } else {
      return true;
    }
  };
  _onPressSignup = () => {
    const { fname, lname, email, password } = this.state;
    if (fname == "") {
      this.setState({ fnameError: "Please enter first name." });
    } else {
      this.setState({ fnameError: "" });
    }
    if (lname == "") {
      this.setState({ lnameError: "Please enter last name." });
    } else {
      this.setState({ lnameError: "" });
    }
    if (email == "") {
      this.setState({ emailError: "Please enter email." });
    } else {
      if (!this.validateEmail(email)) {
        this.setState({ emailError: "Please enter valid email." });
      } else {
        this.setState({ emailError: "" });
      }
    }
    if (password == "") {
      this.setState({ passwordError: "Please enter password." });
    } else {
      this.setState({ passwordError: "" });
    }
    Keyboard.dismiss();
  };
  render() {
    return (
      <View style={styles.loginContainerWrapper}>
        <GeneralStatusBarColor
          backgroundColor="#48B0FE"
          barStyle="light-content"
        />
        <View style={styles.loginContainerTopHalf}>
          <Image
            source={require("../../../assets/logo.png")}
            style={styles.logo}
          />
        </View>
        <View style={styles.loginContainerBottomHalf} />
        <View style={styles.loginBoxContainer}>
          <View style={styles.loginBoxContainerInner}>
            <View style={styles.loginInputSection}>
              <Input
                onChangeText={fname => this.setState({ fname })}
                style={styles.input}
                placeholder="First Name *"
                placeholderTextColor="grey"
                rightIcon={{
                  type: "font-awesome",
                  name: "user-o",
                  size: 15,
                  color: "gray"
                }}
              />
            </View>
            <Text style={styles.errorText}>{this.state.fnameError}</Text>
            <View style={styles.loginInputSection}>
              <Input
                onChangeText={lname => this.setState({ lname })}
                style={styles.input}
                placeholder="Last Name *"
                placeholderTextColor="grey"
                rightIcon={{
                  type: "font-awesome",
                  name: "user-o",
                  size: 15,
                  color: "gray"
                }}
              />
            </View>
            <Text style={styles.errorText}>{this.state.lnameError}</Text>
            <View style={styles.loginInputSection}>
              <Input
                onChangeText={email => this.setState({ email })}
                style={styles.input}
                keyboardType="email-address"
                placeholder="Email *"
                placeholderTextColor="grey"
                rightIcon={{
                  type: "font-awesome",
                  name: "envelope-o",
                  size: 15,
                  color: "gray"
                }}
              />
            </View>
            <Text style={styles.errorText}>{this.state.emailError}</Text>
            <View style={styles.loginInputSection}>
              <Input
                onChangeText={password => this.setState({ password })}
                secureTextEntry={true}
                style={styles.input}
                width={100}
                placeholder="Password *"
                rightIcon={{
                  type: "font-awesome",
                  name: "eye",
                  size: 15,
                  color: "gray"
                }}
                inputContainerStyle={
                  {
                    // borderBottomWidth: 1,
                    // borderColor: "rgba(255,255,255,0.8)"
                    // borderBottomColor: "rgba(255,255,255,0.8)"
                  }
                }
                // underlineColor="white"
              />
            </View>
            <Text style={styles.errorText}>{this.state.passwordError}</Text>
            <TouchableOpacity
              style={styles.loginInputSectionButton}
              underlayColor="#fff"
              backgroundColor="#48B0FE"
              onPress={() => this._onPressSignup()}
            >
              <Text style={styles.loginButtonText}>Sign Up</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  errorText: {
    // flex: 1,
    color: "red",
    textAlign: "left",
    alignSelf: "flex-start",
    padding: 0,
    paddingHorizontal: 25
  },
  signupLink: {
    flex: 1,
    position: "absolute",
    width: "80%"
    // top: 220
  },
  logo: {
    width: 150,
    height: 150
  },
  input: {
    flex: 1,
    padding: 0
  },
  loginInputSection: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "center",
    paddingHorizontal: 15,
    // padding: 5,
    marginBottom: 0
  },
  loginInputSectionPassword: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    top: 0,
    width: "100%",
    paddingHorizontal: 15
  },
  loginButtonText: {
    color: "#fff",
    padding: 10,
    textAlign: "center",
    fontSize: 18
  },
  loginInputSectionButton: {
    flex: 1,
    // width: "100%",
    borderRadius: 50,
    shadowColor: "grey",
    // justifyContent: "center",
    // alignItems: "center",
    // textAlign: "center",
    paddingHorizontal: 70,
    backgroundColor: "#48B0FE",
    marginTop: 20,
    marginBottom: 20
  },
  loginBoxContainer: {
    flex: 1,
    width: "75%",
    position: "absolute",
    zIndex: 9999,
    top: "42%",
    height: "auto",
    // justifyContent: "center",
    // alignItems: "center",
    alignSelf: "center",
    backgroundColor: "#ffffff",
    borderRadius: 5,
    borderWidth: 1,
    borderColor: "rgba(128,128,128,0.1)",
    paddingHorizontal: 10
  },
  loginBoxContainerInner: {
    height: "auto",
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
    marginBottom: 5,
    marginTop: 10
  },
  loginContainerWrapper: {
    flex: 1
  },
  loginContainerTopHalf: {
    flex: 1,
    backgroundColor: "#48B0FE",
    justifyContent: "center",
    alignItems: "center"
  },
  loginContainerBottomHalf: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  }
});
export default SignupScreen;
